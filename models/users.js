const mongoose = require (`mongoose`)

const Schema = mongoose.Schema

require(`mongoose-currency`).loadType(mongoose)
//const currency = mongoose.Types.currency

const commentSchema = new Schema({
    comment: {
        type: String,
        required: true,
    },
    name:{
        type: String,
        required:true,
    }
},{
    timestamps: true,
})


const userSchema = new Schema({
    name:{
        type: String,
        required: true,
    },
    comment:{
        type:String,
    },
    likes:{
        type: Number,
        min:0,
    },
    dislikes:{
        type: Number,
        min:0,
    },
    comments: [commentSchema]
},{
    timestamps: true
})

var User = mongoose.model('User',userSchema)

module.exports = User