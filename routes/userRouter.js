const express = require('express');
const bodyParser = require('body-parser')

const Users = require('../models/users')

const userRouter = express.Router()
userRouter.use(bodyParser.json())

userRouter.route('/')
    .get((req, res, next) => {
        /**Using the mongoose methods */
        Users.find({})
            .then((users) => {
                res.statusCode = 200 /**Inform a http request that it`s all ok */
                res.setHeader(`Content-type`, `application/json`) /**The type of the object */
                res.json(users) /**Returning into Json */
                /**These lines about error it works like this:
                 * if you get an error, this error going to the catch and this error going to be
                 * send to all application to informs that u got an error, an it will going to 
                 * now allow to the application to peform the remainder of the application
                 */
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .post((req, res, next) => {
        /**Extract an information from body */
        Users.create(req.body)
            .then((user) => {
                console.log(`user Created`, user)
                res.statusCode = 200 /**Inform a http request that it`s all ok */
                res.setHeader(`Content-type`, `application/json`) /**The type of the object */
                res.json(user) /**Returning into Json */
            }, (err) => next(err))
            .catch((err) => next(err))
    })
    .put((req, res, next) => {
        /**403 is a code for forbidden method http */
        res.statusCode = 403;
        res.end(`PUT operation not supported on /Users`)
    })
    .delete((req, res, next) => {
        /**Deleting all Users */
        Users.remove({})
            .then((resp) => {
                res.statusCode = 200 /**Inform a http request that it`s all ok */
                res.setHeader(`Content-type`, `application/json`) /**The type of the object */
                res.json(resp) /**Returning into Json */
            }, (err) => next(err))
            .catch((err) => next(err))
    })

/**For each User
 * userID */
userRouter.route('/:userId')
.get((req, res, next) => {
    /**Generating a response with JSON */
    Users.findById(req.params.userId)
        .then((user) => {
            res.statusCode = 200 /**Inform a http request that it`s all ok */
            res.setHeader(`Content-type`, `application/json`) /**The type of the object */
            res.json(user) /**Returning into Json */
        }, (err) => next(err))
        .catch((err) => next(err))
})

.post((req, res, next) => {
    /**Doesn't makes sense do post at Id */
    res.statusCode = 403;
    res.end(`POST operation not supported on /:Users ${req.params.userId}`)
})

.put((req, res, next) => {
    /**Updating a specify user with userId */
    Users.findByIdAndUpdate(req.params.userId, {
            $set: req.body
        }, {
            new: true
        })
        .then((user) => {
            res.statusCode = 200 /**Inform a http request that it`s all ok */
            res.setHeader(`Content-type`, `application/json`) /**The type of the object */
            res.json(user) /**Returning into Json */
        }, (err) => next(err))
        .catch((err) => next(err))
})

.delete((req, res, next) => {
    /**Deleting an specifique user */
    Users.findByIdAndRemove(req.params.userId)
        .then((resp) => {
            res.statusCode = 200 /**Inform a http request that it`s all ok */
            res.setHeader(`Content-type`, `application/json`) /**The type of the object */
            res.json(resp) /**Returning into Json */
        }, (err) => next(err))
        .catch((err) => next(err))
})

/**For the comments into each userId */
userRouter.route('/:userId/comments')
.get((req, res, next) => {
    /**Using the mongoose methods */
    Users.findById(req.params.userId)
        .then((user) => {
            /**If the user exists */
            if (user) {
                res.statusCode = 200 /**Inform a http request that it`s all ok */
                res.setHeader(`Content-type`, `application/json`) /**The type of the object */
                res.json(user.comments) /**Returning into Json */
                /**These lines about error it works like this:
                 * if you get an error, this error going to the catch and this error going to be
                 * send to all application to informs that u got an error, an it will going to 
                 * now allow to the application to peform the remainder of the application
                 */
            } else {
                /**If the user doesn't exists */
                err = new Error(`user ${req.params.userId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the user */
                return next(err)
            }
        }, (err) => next(err))
        .catch((err) => next(err))
})

.post((req, res, next) => {
    /**Extract an information from body */
    /**Using the mongoose methods */
    Users.findById(req.params.userId)
        .then((user) => {
            /**If the user exists */
            if (user) {
                /**Push the information to the body of the object */
                user.comments.push(req.body)
                /**Saving the comment */
                user.save()
                    /**And the after save, we return the updated user to user  */
                    .then((user) => {
                        res.statusCode = 200
                        res.setHeader('Content-Type', 'application/json')
                        res.json(user)
                    }, (err) => next(err))

            } else {
                /**If the user doesn't exists */
                err = new Error(`user ${req.params.userId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the user */
                return next(err)
            }
        }, (err) => next(err))
        .catch((err) => next(err))
})

.put((req, res, next) => {
    /**403 is a code for forbidden method http */
    res.statusCode = 403;
    res.end(`PUT operation not supported on /Users/${req.params.userId}/comments`)
})

.delete((req, res, next) => {
    /**Deleting all Users */
    Users.findById(req.params.userId)
        .then((user) => {
            /**If the user exists */
            if (user) {
                /**Do a for to delete each comment one by one looking into array of comments*/
                for (var i = (user.comments.length - 1); i >= 0; i--) {
                    user.comments.id(user.comments[i]._id).remove()
                }
                /**Saving the deleted */
                user.save()
                    /**And the after save, we return the updated user to user  */
                    .then((user) => {
                        res.statusCode = 200
                        res.setHeader('Content-Type', 'application/json')
                        res.json(user)
                    }, (err) => next(err))
            } else {
                /**If the user doesn't exists */
                err = new Error(`user ${req.params.userId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the user */
                return next(err)
            }
        }, (err) => next(err))
        .catch((err) => next(err))
})

/**For each Comment
* CommentID */
userRouter.route('/:userId/comments/:commentId')
.get((req, res, next) => {
    /**Generating a response with JSON */
    Users.findById(req.params.userId)
        .then((user) => {
            /**Check if the user exists and the comment isn't null */
            if (user != null && user.comments.id(req.params.commentId) != null) {
                res.statusCode = 200 /**Inform a http request that it`s all ok */
                res.setHeader(`Content-type`, `application/json`) /**The type of the object */
                res.json(user.comments.id(req.params.commentId)) /**Returning into Json */
                /**These lines about error it works like this:
                 * if you get an error, this error going to the catch and this error going to be
                 * send to all application to informs that u got an error, an it will going to 
                 * now allow to the application to peform the remainder of the application
                 */
            } else if (!user) {
                /**If the user doesn't exists */
                err = new Error(`user ${req.params.userId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the user */
                return next(err)
            } else {
                /**If the comment doesn't exists */
                err = new Error(`Comment ${req.params.commentId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the user */
                return next(err)
            }
        }, (err) => next(err))
        .catch((err) => next(err))
})

.post((req, res, next) => {
    /**Doesn't makes sense do post at Id */
    res.statusCode = 403;
    res.end(`POST operation not supported on /Users/${req.params.userId}/comments/${req.params.commentId}`)
})

.put((req, res, next) => {
    /**Updating a specify user with dishId */
    /**Generating a response with JSON */
    Users.findById(req.params.dishId)
        .then((dish) => {
            /**Check if the dish exists and the comment isn't null */
            if (dish != null && dish.comments.id(req.params.commentId) != null) {
                /**Updating a rating */
                if (req.body.rating) {
                    dish.comments.id(req.params.commentId).rating = req.body.rating
                }
                /**Updating comment */
                if (req.body.comment) {
                    dish.comments.id(req.params.commentId).comment = req.body.comment
                }

                /**Saving the comment */
                dish.save()
                    /**And the after save, we return the updated dish to user  */
                    .then((dish) => {
                        res.statusCode = 200
                        res.setHeader('Content-Type', 'application/json')
                        res.json(dish)
                    }, (err) => next(err))
            } else if (!dish) {
                /**If the dish doesn't exists */
                err = new Error(`Dish ${req.params.dishId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the dish */
                return next(err)
            } else {
                /**If the comment doesn't exists */
                err = new Error(`Comment ${req.params.commentId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the dish */
                return next(err)
            }
        }, (err) => next(err))
        .catch((err) => next(err))
})

.delete((req, res, next) => {
    /**Deleting a specifique comment */
    Users.findById(req.params.dishId)
        .then((dish) => {
            /**If the dish exists */
            if (user != null && user.comments.id(req.params.commentId) != null) {
                /**Do a for to delete a specifique comment*/
                user.comments.id(req.params.commentId).remove()

                /**Saving the deleted */
                user.save()
                    /**And the after save, we return the updated user to user  */
                    .then((user) => {
                        res.statusCode = 200
                        res.setHeader('Content-Type', 'application/json')
                        res.json(user)
                    }, (err) => next(err))
            } else if (!user) {
                /**If the user doesn't exists */
                err = new Error(`user ${req.params.userId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the user */
                return next(err)
            } else {
                /**If the comment doesn't exists */
                err = new Error(`Comment ${req.params.commentId} not found`)
                err.status = 404
                /*This return goes to app.js to return into the render error page to informe that
                you got an error with the user */
                return next(err)
            }
        }, (err) => next(err))
        .catch((err) => next(err))
})

module.exports = userRouter;
